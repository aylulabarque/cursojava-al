import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Pantalla5 {

	private JFrame frame;
	private JTextField txt;
	private JLabel lblCategoria;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla5 window = new Pantalla5();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla5() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(127, 255, 212));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Inresar Categoria");
		lblNewLabel.setFont(new Font("Lucida Grande", Font.ITALIC, 16));
		lblNewLabel.setBounds(35, 67, 136, 20);
		frame.getContentPane().add(lblNewLabel);
		
		txt = new JTextField();
		txt.setBounds(203, 65, 130, 26);
		frame.getContentPane().add(txt);
		txt.setColumns(10);
		
		JButton btnNewButton = new JButton("Calcular");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char hijo = 'a';
				char padres = 'b';
				char abuelos = 'c';
				
				String h = Character.toString(hijo);
				String p = Character.toString(padres);
				String c = Character.toString(abuelos);
				
				String categoria = txt.getText();
				
				if (categoria.equals(h)) {
					lblCategoria.setText("Hijo");}
				 
				else if (categoria.equals(p))
					lblCategoria.setText("Padres");
				
				else 
					lblCategoria.setText("Abuelos");
				
				
				
			}
				
			});
	
		
		btnNewButton.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
		btnNewButton.setBounds(38, 131, 119, 38);
		frame.getContentPane().add(btnNewButton);
		
		lblCategoria = new JLabel("Categoria");
		lblCategoria.setFont(new Font("Lucida Grande", Font.PLAIN, 18));
		lblCategoria.setBackground(new Color(221, 160, 221));
		lblCategoria.setOpaque(true);
		lblCategoria.setBounds(191, 135, 222, 27);
		frame.getContentPane().add(lblCategoria);
	}
}
